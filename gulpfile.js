var elixir = require('laravel-elixir');

// https://github.com/nathanmac/laravel-elixir-imagemin
// require('laravel-elixir-imagemin');

// https://github.com/Vivify-Ideas/laravel-elixir-ng-annotate
require('laravel-elixir-ng-annotate');

var config = elixir.config;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
  mix.copy('node_modules/bootstrap-sass/assets/fonts/bootstrap/**', 'public/fonts/');
  mix.copy('resources/assets/fonts/**', 'public/fonts/');
  mix.copy('resources/views/**/*.html', 'public/');
  mix.copy('resources/assets/images/**', 'public/images/');
  //mix.copy('./resources/assets/js/angular-schema-form-ui-datepicker/**/*.html', './public/angular-schema-form-ui-datepicker/');

  mix.sass('screen.scss');
  mix.scripts([
      './node_modules/fabric/dist/fabric.js',
      './node_modules/jquery/dist/jquery.js',
      './node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
      './node_modules/lodash/lodash.js',
      './node_modules/mobilelocker-tracking/legacy.js',
      'jquery.form.js',
      'jquery.maskedinput.min.js',
      'jquery.validate.min.js',
      'purl.js'
    ],
    './public/js/vendors.js');
  mix.scripts(['therakos.mir.form.js'], './public/js/app.js');

  mix.browserSync({
    files: [
      config.get('public.css.outputFolder') + '/**/*.css',
      config.get('public.js.outputFolder') + '/**/*.js',
      //config.get('public.versioning.buildFolder') + '/rev-manifest.json',
      config.get('public.js.outputFolder') + '/**/*.html',
      'resources/views/**/*.html'
    ],
    reloadDelay: 2000,
    open: false,
    server: {
      baseDir: './public'
    },
    proxy: null
  });
});
