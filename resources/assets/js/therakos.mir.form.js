$(document).ready(function () {
  initApp();
});
function initApp() {
  initSignatureCapture();
  initForm();
  var d = new Date();
  $('#date').html(d.toDateString());
}

function mirAlert(message) {
  $('#alert-window, #alert-window-overlay').remove();
  var windowOverlayJQ = $('<div id="alert-window-overlay" style="z-index:100000;height:100%;width:100%;position:fixed;top:0;left:0;"></div>');
  $('body').append(windowOverlayJQ);

  //var top = Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px";
  //var left = Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px";

  var windowJQ = $('<div id="alert-window"></div>');
  $(windowJQ).attr("style", "z-index:100001;background:#737373;color: #FFFFFF;min-width:250px;height:auto;position:fixed;top:40%;text-align:center;left:40%;border-radius:4px;padding:15px; ");
  $(windowJQ).append('<p style="padding:10px 0;">' + message + '</p>');
  $(windowJQ).append('<p><button class="btn btn-success" onclick="$(\'#alert-window, #alert-window-overlay\').remove();">OK</button></p>');
  $('body').append(windowJQ);
}

function initForm() {
  $("#phone").mask("999-999-9999");

  // US/Canada Toggle
  $('select#country').change(function () {
    var that = this;
    // hide all countries
    $('div.country').each(function (index, elem) {
      $('select', $(elem)).removeAttr("required");
      $('input', $(elem)).removeAttr("required");
      $(elem).hide();
    });

    // show country dropdowns
    $('div.country.' + $(this).val().toLowerCase()).removeClass("hidden");
    $('div.country.' + $(this).val().toLowerCase()).show();
    $('div.country.' + $(this).val().toLowerCase() + ' select').attr("required", "required");
    $('div.country.' + $(this).val().toLowerCase() + ' input').attr("required", "required");
  });

  //@see documentation http://jqueryvalidation.org/validate
  $('#mir-form').validate({
    ignore: '.ignore',
    highlight: function (element, errorClass) {
      $(element).parent('.form-group').addClass('has-error');
    },
    unhighlight: function (element, errorClass) {
      $(element).parent('.form-group').removeClass('has-error');
    },
    submitHandler: function (form) {
      if ($('select#country').val() == "USA") {
        // blank out canada
        $('select#province').val("");
        $('input#postal_code').val("");
      }
      else if ($('select#country').val() == "Canada") {
        // blank out canada
        $('select#state').val("");
        $('input#zip_code').val("");
      }

      var formData = $(form).serializeArray();
      //console.log('serialized formData', formData);
      mobilelocker.submitForm('mir-form', formData);
      // TMIR-3 - load a new form
      setTimeout(function () {
        // show without page url as title
        mirAlert('Thank you!');
        mobilelocker.synchronize();
        clearSignature();
        //$('#signature_pad-clear').trigger('click');
        $(this).resetForm();
        $(form)[0].reset();
      }, 1000);
      return false;
    },
    success: function (label) {
      $(label).parent('.form-group').addClass('has-success');
    },
    rules: {
      /*
       zip_code: {
       required: ($('select#country').val() === "USA"),
       digits: true
       },
       postal_code: {
       required: ($('select#country').val() === "Canada"),
       },
       */
    },
    messages: {
      //zip_code: 'Required'
    }
  });

  $('#signatureData').rules('add', {
    required: true,
    minlength: 200,
    messages: {
      required: 'Your signature is required',
      minlength: 'Please sign your signature.'
    }
  });

  /*$('#mir-form').submit(function() {
   formData = $(this).formSerialize();
   captured = mobilelocker.captureData('mir-form', formData);
   if (captured) {
   mirAlert('Thank you!');
   mobilelocker.captureScreenshot('mir-form-screenshot', 'mir-form');
   }
   return false;
   });*/
}

var signatureCanvas = null;

function clearSignature(e) {
  console.log('clearSignature()');
  $('#signatureData').val(null);
  signatureCanvas.clear();
}

function initSignatureCapture() {
  console.log('initSignatureCapture()');
  var wrapper = $('.SignaturePad__Wrapper');
  var width = wrapper.innerWidth();
  var height = 200;
  $('#signaturePad').attr('width', width).attr('height', height);

  var canvas = new fabric.Canvas('signaturePad', {
    isDrawingMode: true
  });
  canvas.freeDrawingBrush = new fabric['PencilBrush'](canvas);
  canvas.freeDrawingBrush.color = '#000000';
  canvas.freeDrawingBrush.width = 5;
  signatureCanvas = canvas;
  canvas.on('mouse:up', function (options) {
    console.log('rendering signature to PNG');
    var png = canvas.toDataURL({});
    $('#signatureData').val(png);
  });
}
